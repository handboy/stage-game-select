var dados;
var faseAtual = 0;

$(document).ready(function () {

  var flickerAPI = "js/arquivo.json";

  $.getJSON( flickerAPI)
    .done(function( data ) {
      dados = data;
        for (var i = 0; i < data.fases.length; i++) {
          var apHtml = '<div class="fase">';
          if(!data.fases[i].lock){
             $("#fases").append(apHtml + '<a class="gameLink" href="#jogo" onclick="goGame('+i+');">'
              +'<img class="cadeado"></a></div>');
          } else {
            $("#fases").append(apHtml + '<a class="gameLink not-active" href="#jogo" onclick="goGame('+i+');">'
              +'<img class="cadeado" src="img/lock.png"></a></div>');
          }
         
        };
    })

    .fail(function(){
        console.log( "Não foi possivel encontrar o json" );
    });


    var results = new RegExp('[\?&amp;]game=([^&amp;#]*)').exec(window.location.href);
    if(results != null){
      console.log(results[1]);
    }

   
    $("#jogo").slideUp();
});



function callFunc(arg){
  var argumentos = [];

  for(var key in dados.fases[arg].valor){
    argumentos.push(dados.fases[arg].valor[key]);
  }

  $('#myFrame')[0].contentWindow.c2_callFunction("beginGame",[argumentos.toString()]);  
}

function goGame(arg){
  var valor = arg;
  $( "#fases" ).slideUp( "slow", function() {
    //alert(valor);
    $("#jogo").slideDown();
    callFunc(valor);
  });
}

function endGame(arg){
  if(arg){
    faseAtual++;
    $('.fase').children().eq(faseAtual).removeClass("not-active" );
    $('.fase').children().eq(faseAtual).find("img").removeAttr('src');
  }

  $( "#jogo" ).slideUp( "slow", function() {
    //alert(valor);
    $("#fases").slideDown();

  });  
  
}

//Função para enviar o log para a plataforma.
function sessaoLog(userId, jogoId, tipoLogId, data) {
  var secao = {
      "usuario_id": userId,
      "jogo_id": jogoId,
      "tipo_log_id": tipoLogId,
      "info": data,
  };

  var jqxhr = $.post("http://localhost/moodle/gamedu/secao/add.json", secao)
  .done(function(data) {
    console.log("Post Enviado ");
  })
  .fail(function(XMLHttpRequest, textStatus, errorThrown) {
    console.log( "Post error" + textStatus + " " +  errorThrown);
  });
}
